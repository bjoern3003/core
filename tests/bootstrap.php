<?php

declare(strict_types=1);

define('PFAD_ROOT', dirname(__DIR__) . '/');
define('URL_SHOP', 'http://localhost/');
require_once __DIR__ . '/../includes/defines.php';
require_once __DIR__ . '/../includes/autoload.php';

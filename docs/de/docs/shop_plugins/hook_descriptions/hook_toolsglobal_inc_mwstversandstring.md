# HOOK_TOOLSGLOBAL_INC_MWSTVERSANDSTRING (103)

## Triggerpunkt

Nach dem Ermitteln des Versandkosten-MwST-Strings, befor dieser String zurückgegeben wird

## Parameter

* `string` **&cVersandhinweis** - Versandhinweis
* `JTL\Catalog\Product\Artikel` **oArtikel** - Artikel-Objekt
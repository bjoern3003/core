# HOOK_WARENKORB_CLASS_FUEGEEIN (126)

## Triggerpunkt

Nach dem Hinzufügen eines Artikels, zum Warenkorb

## Parameter

* `int` **kArtikel** - Artikel-ID
* `array` **&oPosition_arr** - Positionen-array
* `float` **&nAnzahl** - hinzuzufügende Artikelanzahl
* `bool` **exists** - (default `= true`)
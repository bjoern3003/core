# HOOK_BACKEND_FUNCTIONS_GRAVATAR> (227)

## Triggerpunkt

Nach dem Laden eines Gravatar-Bildes

## Parameter

* `string` **&url** - URL zum Gravatar-Bild
* `AdminAccount` **&oAdminAccount** - *_SESSION*-Parameter "`AdminAccount`"